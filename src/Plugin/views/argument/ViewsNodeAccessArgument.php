<?php

namespace Drupal\views_node_access_grants\Plugin\views\argument;

/**
 * Defines a filter for node access grants.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("views_node_access_grants_arg")
 */
class ViewsNodeAccessArgument extends NumericArgument {

  /**
   * {@inheritdoc}
   */
  public function setArgument($arg) {
  
    

  }

}