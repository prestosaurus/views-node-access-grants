<?php

/**
 * @file
 * Definition of Drupal\views_node_access_grants\Plugin\views\field\ViewsNodeAccessField
 */

namespace Drupal\views_node_access_grants\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Field handler to flag the User Active Status.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_node_access_field")
 */
class ViewsNodeAccessField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    // Get current user ID.
    $currentUserID = \Drupal::currentUser()->id();

    $database = \Drupal::database();

    $results = $database
      ->select('node_access', 'n')
      ->fields('n', ['gid'])
      ->condition('gid', 0, '>')
      // ->condition('gid', $currentUserID, '=')
      ->condition('nid', $values->nid, '=')
      ->execute()
      ->fetch();

    $render = $results->gid;
    return $render;

  }

}
