<?php

/**
 * @file
 * Definition of Drupal\views_node_access_grants\Plugin\views\filter\ViewsNodeAccessFilter.
 */

namespace Drupal\views_node_access_grants\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filters nodes by access grants.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("views_node_access_filter")
 */
class ViewsNodeAccessFilter extends FilterPluginBase {

  /**
   * See _node_access_where_sql() for a non-views query based implementation.
   */
  public function query() {

    $currentUserID = \Drupal::currentUser()->id();

    $configuration = [
      'table' => 'node_access',
      'field' => 'nid',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'operator' => '='
    ];

    $join = Views::pluginManager('join')
      ->createInstance('standard', $configuration);

    $this->query
      ->addRelationship('node_access', $join, 'node_field_data');

    $this->query
      ->addWhere('AND', 'node_access.gid', $currentUserID);

  }

}