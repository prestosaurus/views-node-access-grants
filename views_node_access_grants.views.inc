<?php

/**
 * Implements hook_views_data_alter().
 */
function views_node_access_grants_views_data_alter(array &$data) {
  $data['node']['views_node_access_grants'] = [
    'title' => t('Has Node Access Grants'),
    'field' => [
      'title' => t('Has Node Access Grants'),
      'help' => t('Node access gid field.'),
      'id' => 'views_node_access_field',
    ],
    'filter' => [
      'title' => t('Has Node Access Grants Filter'),
      'help' => t('Node access gid filer.'),
      'id' => 'views_node_access_filter',
    ],
  ];
}